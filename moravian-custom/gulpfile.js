'use strict';

require('dotenv').config()
const { spawn } = require('child_process');
const { watch } = require('gulp');

function runCommand(packageName, args) {
    var result = '';
    var clRes = '';
    var err = '';
    var build = spawn(packageName, args, { shell: true });
    build.stdout.on('data', data => {
        result += data.toString();
        console.log(`stdout: ${result}`);
    });

    build.stderr.on('data', data => {
        err += data.toString();
        console.error(`stderr: ${err}`);
    });

    build.on('close', code => {
        code += code.toString();
        console.log(`child process exited with code ${code}`);
    });
    return build;
}

function javascript() {
    return runCommand('npm', ['run', 'compile:js']);
}

function scss() {
    return runCommand('npm', ['run', 'compile:scss']);
}

module.exports = {
    default: function () {
        runCommand('npm', ['run', 'compile:js']);
        runCommand('npm', ['run', 'compile:scss']);
        runCommand('npm', ['run', 'compile:fonts']);
        runCommand('npm', ['run', 'compile:plugins']);
        watch('**/client/**/*.scss', scss);
        watch('**/client/**/*.js', javascript);
    }
}