# Moravian Internship Program (MIP)

This is a repository for the Moravian Internship Program application.

# Build Setup

## Set Node Version

### Add global node version switcher `n`:

```bash
sudo npm install n -g;
```

### Install node 14.17.6:

```bash
sudo n 14.17.6;
```

### Install the correct version of Node-Sass

```bash
npm install node-sass
```

## Setup SFRA

Follow the steps in the SFRA read me at `./storefront-reference-architecture/README.md`

## Build Code

Add dw.json file in `./moravian-custom` directory like so:

```json
{
    
  "hostname": "xxx.sandbox.us01.dx.commercecloud.salesforce.com",
  "username": "my.email@whatever.com",
  "password": "xxxxxxxx",
  "code-version": "version1",
  "cartridgesPath": ""
}
```

Change Directories to `./moravian-custom` and run:

```bash
npm install;
npm run develop
```